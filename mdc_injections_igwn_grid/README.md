# MDC injections on the grid

This page gives detailed information on how to run BayesWve on MDCs on the grid
and, further down, walks through the worked example with `bayeswave_pipe`
provided in this repository.

## Executive summary

 * Create tarballs called `mdcs.tar.bz2` for each of the MDC directories.
 * Ask @james-clark to pre-stage tarballs of MDC frames to an HTTP server.
 * Use BayesWave from the conda envs in CVMFS.
 * Call `bayeswave_pipe` with `--osg-deploy`.
 * Set `extra-files = https://some-server/data/MDC-set/mdcs.tar.bz2` to pull all the MDCs of a given set onto the worker node.

## Introduction

Generally speaking, there is no longer any fundamental difference for most
BayesWave jobs when running on the grid.  We use file transfers everywhere
and production-level analyses should always be running from centrally-managed
conda environments which are available in CVMFS.

Basic requirements are:

1. Use software from conda or singularity images in CVMFS.
1. Read data from CVMFS paths.
1. Use HTCondor file transfers.
1. Force post-processing jobs to run in the local HTCondor pool, as needed.
1. Enumerate any additional files for transfer in the `config.ini`

To use BayesWave in CVMFS for a production analysis, set in the `config.ini`:

```ini
[engine]
install-path=/cvmfs/oasis.opensciencegrid.org/ligo/sw/conda/envs/igwn-py37/bin/
bayeswave=%(install-path)s/BayesWave
bayeswave_post=%(install-path)s/BayesWavePost
megaplot=%(install-path)s/megaplot.py
megasky=%(install-path)s/megasky.py
```

To read data from the appropriate CVMFS location *and* get post-processing to
stay local, make sure you have a valid proxy and call `bayeswave_pipe` with:

```console
$  ligo-proxy-init albert.einstein
$  bayeswave_pipe --osg-deploy <other arguments>
```

All jobs use HTCondor file transfer for standard I/O by default.  To add
additional files, use:

```ini
[condor]
extra-files = /some/path/to/extra/data
```

and the file or directory `data` will land in the top level of the job's
working directory.

## MDC-specific changes

To date, MDCs have been analysed on the same cluster they reside on.  To
analyse on the grid:

1. The MDC frames must be transferred with the jobs.
1. The MDC cache file must be modified to point at MDC frame files in the jobs'
   sandboxes, *not* your `/home`.

### Transferring the MDC frames

The optimal way to do this would be for the pipeline to identify the MDC frame
associated with each job.  That is not currently supported.

One easy brute force solution would be to simply add the parent directory for
the MDCs to `extra-files` in the `config.ini` like:

```ini
[condor]
extra-files = /home/gergely.dalya/public_html/O3_allsky/BW_final/s18_UNDD
```

However, if there are a large number of (`>`a few) files in that directory,
this will quickly swamp the network on the submit host so **this should be
avoided.**

A better solution is to create a tarball from the MDC directory and extract it
prior to execution.  BayesWave jobs execute a small auto-generated python
script `setupdirs.py` prior to calling BayesWave (as an HTCondor
[PreCmd](https://htcondor.readthedocs.io/en/latest/classad-attributes/job-classad-attributes.html#job-classad-attributes)).
This script looks for a file `mdcs.tar.bz2` and extracts the contents if it is
present.

To set this up:

Tarball the MDC directory: 

```
tar -cjf mdcs.tar.bz2 s18_UNDD
```
**IMPORTANT NOTE**: Please create the tarball so that it contains only the
final directory before the MDC frames!

Transfer the tarball with the job:

```
[condor]
extra-files = mdcs.tar.bz2
```

Note that the name of this tarball is hardcoded! If it is named something else,
the script will ignore it.

#### Pre-staged tarballs: preferred option

If 100s or 1000s of jobs start simultaneously, this will still be a heavy load
on the network on the submit machine.  The mitigate this, we can stage the
tarballs to an external webserver and transfer the data over HTTP: this will
"only" induce a network load on the individual worker nodes.

To use this option:

 1. Create the appropriately named tarball `mdcs.tar.bz2` *for a given MDC directory*.
 1. Ask someone (@james-clark) to stage that data to an HTTP server and tell
    them what the parent directory is called.
 1. Set `extra-files` to point at the corresponding URL.

E.g.: transferring and MDC tarball from an HTTP server:

```ini
[condor]
extra-files = https://igwn-data.nautilus.optiputer.net/data/allsky_mdcs/s18_UNDD/mdcs.tar.bz2
```

### Modifying the MDC cache file

The original cache file includes an absolute path to the local fileystem.  This
must be changed to point to the location of the frame files in the job sandbox
*relative to the BayesWave working directory*.

E.g., for the case where MDC frames live in a directory `s18_UNDD` in the top
level of the jobs' sandboxes, modify the `MDC.cache` **from** e.g.:

```console
L1H1 SN_s18_0999 1240803914 100 file://localhost/home/gergely.dalya/public_html/O3_allsky/BW_final/s18_UNDD/L1H1-SN_s18_0999-1240803914-100.gwf
```

**to**:

```console
L1H1 SN_s18_0999 1240803914 100 ../s18_UNDD/L1H1-SN_s18_0999-1240803914-100.gwf
```

(i.e., strip off the leading absolute prefix and replace with the relative path
 to the directory as it appears to the job)


## Walkthrough

Suppose you want to analyse the MDC set in `/home/gergely.dalya/public_html/O3_allsky/BW_final/s18_UNDD/`

First, create a tarball from the **immediate parent directory** of the frame
files.  **IMPORTANT**: It is critical to get the directory levels correct, in
order that the relative-path MDC cache file makes sense:

```console
# Start from just one directory up:
$  pwd
/home/gergely.dalya/public_html/O3_allsky/BW_final
$  tar -cjf mdcs.tar.bz2 s18_UNDD
```

James will upload this tarball to `https://igwn-data.nautilus.optiputer.net/data/allsky_mdcs/s18_UNDD/mdcs.tar.bz2`:

Next, make sure you're using BayesWave from the IGWN conda environment in CVMFS:

```ini
[engine]
install-path=/cvmfs/oasis.opensciencegrid.org/ligo/sw/conda/envs/igwn-py37/bin/
```

Add the URL for the MDC tarball to `extra-files`:

```ini
[condor]
extra-files = https://igwn-data.nautilus.optiputer.net/data/allsky_mdcs/s18_UNDD/mdcs.tar.bz2
```

Modify your original MDC cache file to use relative paths (see e.g. [`fix-cache.sh`]()):

```
file://localhost/home/gergely.dalya/public_html/O3_allsky/BW_final/s18_UNDD/L1H1-SN_s18_0999-1240803914-100.gwf
->
../s18_UNDD/L1H1-SN_s18_0999-1240803914-100.gwf
```

Make sure your `config.ini` points at the updated cache (!)

```ini
[injections]
mdc-cache = /home/albert.einstein/new_MDC.cache
```

Generate the workflow (you will probably have to reinstall `BayesWaveUtils`
from the head of master to get the latest required additions for this):

```console
bayeswave_pipe config.ini \
    --workdir ${workdir} \
    --server ${LIGO_DATAFIND_SERVER} \
    --skip-megapy \
    --trigger-list trigtimes.txt \
    --osg-deploy
```

Submit the workflow **from `ldas-osg.ligo.caltech.edu`** (or the `-wa`/`-la`
equivalents but mileage there may vary)

See also the example workflow in this directory (based on the DAG in
`/home/gergely.dalya/public_html/O3_allsky/BW_final/jobs_s18_100100.00`):

* The MDCs for `s18_UNDD` are already in place on the HTTP server.
* Example [`config.ini`](https://git.ligo.org/james-clark/bayeswave-use-cases/-/blob/master/mdc_injections_igwn_grid/config.ini)
* [Pipeline
command](https://git.ligo.org/james-clark/bayeswave-use-cases/-/blob/master/mdc_injections_igwn_grid/run_pipe.sh)
(**Note**: I had to copy Gergely's cache files for this example - you should be able to ignore things below the `XXX`)

## Staging data

(Notes mostly for @james-clark)

Use the nginx deployment in the ligo-rucio namespace in k8s on Nautilus.

1 GB PVC mounted @ `/usr/share/nginx/html/data` (will probably need to bump this up)

See k8s directory for more details.

Copy data with e.g.:
```
$  kubectl cp ./mdcs.tar.bz2 nginx-7f87645946-jrpg4:/usr/share/nginx/html/data
```

Target URL is then: 
```
https://igwn-data.nautilus.optiputer.net/data/mdcs.tar.bz2
```

### Horizontal autoscaling guide

https://kubernetes.io/docs/tasks/run-application/horizontal-pod-autoscale-walkthrough/
