#!/bin/bash -e
BAYESWAVE_PIPE=/home/james.clark/opt/lscsoft/bayeswave/bin/bayeswave_pipe

${BAYESWAVE_PIPE} config.ini \
    --workdir mdc_injection_demo \
    --skip-post \
    --skip-megapy \
    --trigger-list trigtimes.txt \
    --osg-deploy

# XXX You will not need anything below here if you regenerate your cache files XXX

# Copy the frame cache files into place because this trigger list doesn't work
# otherwise
cp /home/gergely.dalya/public_html/O3_allsky/BW_final/jobs_s18_100100.00/datafind/{H1,L1}.cache mdc_injection_demo/datafind

# Update frame paths for CVMFS
oldprefix="/hdfs"
newprefix="/cvmfs/oasis.opensciencegrid.org/ligo"
sed -i "s|${oldprefix}|${newprefix}|" ${workdir}/datafind/{H1,L1}.cache

