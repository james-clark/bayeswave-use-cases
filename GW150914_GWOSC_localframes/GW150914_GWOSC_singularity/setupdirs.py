#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2018-2019 James Clark <james.clark@ligo.org>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
"""Compute element hostname.

Creates the output directory for a BayesWave job and prints the name of the
GLIDEIN gatekeeper for OSG jobs.  Prints the name of the local host if not a
GLIDEIN.

The hostname identification comes from:
    https://opensciencegrid.org/user-school-2017/#materials/day2/part1-ex3-submit-osg/

Note that we could use many more env variables for similar purposes if needed.
"""

import re
import os,sys
import socket

#
# Preliminary setup
#
outputDir=sys.argv[1]
executable=sys.argv[2]
if not os.path.exists(outputDir): os.makedirs(outputDir)
geolocation=open("{0}/{1}_cehostname.txt".format(outputDir, executable), "w")

#
# Geolocation
#
machine_ad_file_name = os.getenv('_CONDOR_MACHINE_AD')
try:
    machine_ad_file = open(machine_ad_file_name, 'r')
    machine_ad = machine_ad_file.read()
    machine_ad_file.close()
except TypeError:
    host = socket.getfqdn()+"\n"
    geolocation.writelines(host)
    geolocation.close()
    exit(0)

try:
    host = re.search(r'GLIDEIN_Gatekeeper = "(.*):\d*/jobmanager-\w*"', machine_ad, re.MULTILINE).group(1) 
except AttributeError:
    try:
        host = re.search(r'GLIDEIN_Gatekeeper = "(\S+) \S+:9619"', machine_ad, re.MULTILINE).group(1)
    except AttributeError:
        host = socket.getfqdn()
geolocation.writelines(host+"\n")
geolocation.close()
exit(0)

