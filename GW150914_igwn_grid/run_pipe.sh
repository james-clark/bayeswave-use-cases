#!/bin/bash -e
LABEL=GW150914_IGWN
TRIGTIME=1126259462.42
BAYESWAVE_PIPE=/cvmfs/oasis.opensciencegrid.org/ligo/sw/conda/envs/igwn-py37/bin/bayeswave_pipe
LIGO_DATAFIND_SERVER=datafind.ligo.org:443

for deploy in conda singularity; do 
    config=${LABEL}_${deploy}.ini
    cp ${config} tmp.ini
    sed -i "s/ACCOUNTING_USER/${USER}/" tmp.ini

    workdir=$(echo ${config} | sed -e "s/.ini//g")

    ${BAYESWAVE_PIPE} tmp.ini \
        --workdir ${workdir} \
        --server ${LIGO_DATAFIND_SERVER} \
        --skip-megapy \
        --trigger-time ${TRIGTIME} \
        --osg-deploy

    # force glidein
    sed -i '/requirements/s/$/ \&\& (IS_GLIDEIN=?=True)/' ${workdir}/bayeswave.sub

done
rm -f tmp.ini
