#!/bin/bash -e
TRIGTIME=1126259462.42
BAYESWAVE_PIPE=/cvmfs/oasis.opensciencegrid.org/ligo/sw/conda/envs/igwn-py37/bin/bayeswave_pipe
LIGO_DATAFIND_SERVER=datafind.ligo.org:443

config="GW150914_IGWN_singularity.ini"

workdir=$(echo ${config} | sed -e "s/.ini//g")

${BAYESWAVE_PIPE} ${config} \
    --workdir ${workdir} \
    --server ${LIGO_DATAFIND_SERVER} \
    --trigger-time ${TRIGTIME} \
    --osg-deploy

# Periodic release for jobs held due to bad CVMFS mounts
for sub in ${workdir}/*sub; do
    sed -i '/notification/i \
periodic_release = (HoldReasonCode == 45) && (HoldReasonSubCode == 0) \
job_machine_attrs = Machine \
job_machine_attrs_history_length = 2' ${sub}
done

rm -f tmp.ini
